package org.gtri.elsys.ahib.networktest

import mu.KLogging
import java.io.BufferedReader
import java.io.InputStreamReader

class HostnameRunnable : Runnable {
    companion object: KLogging()

    constructor(newHostname: String)
    {
        val currentHostname: String = getCurrentHostname()
        if(!currentHostname.equals(newHostname))
        {
            logger.info { "Changing device hostname from $currentHostname to $newHostname" }
            setHostname(newHostname)
        }
        else
        {
            logger.info { "Device hostname is already $currentHostname"}
        }
    }

    override fun run() {
        //val currentHostname = getCurrentHostname()
    }

    fun runShellCommand(command: String): String {
        val commands: List<String> = command.split(' ')
        var process: Process = ProcessBuilder(commands).redirectErrorStream(true).start()
        val stdout: BufferedReader = BufferedReader(InputStreamReader(process.inputStream))

        process.waitFor()

        return stdout.readLine() ?: ""
    }

    fun getCurrentHostname(): String {
        val hostname = runShellCommand("getprop net.hostname")
        return hostname
    }

    fun setHostname(hostname: String) {
        runShellCommand("setprop net.hostname " + hostname.toString())

        // Got to cycle WiFi after changing the hostname for it to take effect
        runShellCommand("svc wifi disable")
        runShellCommand("svc wifi enable")
    }
}