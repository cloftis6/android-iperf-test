package org.gtri.elsys.ahib.networktest

import android.os.Bundle
import android.app.Fragment
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.preference.PreferenceManager
import android.text.method.ScrollingMovementMethod
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_main.*
import mu.KLogging
import java.net.InetSocketAddress
import java.net.Socket


class MainFragment : Fragment(), View.OnClickListener {
    companion object: KLogging()

    private var mTestInProgress: Boolean = false
    private var iperfTCPRunnable: IperfRunnable? = null
    private var iperfTCPThread: Thread? = null
    private var iperfUDPRunnable: IperfRunnable? = null
    private var iperfUDPThread: Thread? = null
    private var rssiThread: Thread? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View? = inflater!!.inflate(R.layout.fragment_main, container, false)
        view!!.findViewById<Button>(R.id.buttonStart).setOnClickListener(this)

        view.findViewById<TextView>(R.id.textViewOutput).movementMethod = ScrollingMovementMethod()

        // Start a thread to monitor the RSSI and update it in the UI
        if (rssiThread?.isAlive != true) {
            rssiThread = Thread(Runnable {
                var lastRSSI: Int = Int.MIN_VALUE
                while (true) {
                    Thread.sleep(1000)
                    val wifi: WifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
                    val rssi: Int = wifi.connectionInfo.rssi

                    if (textViewRSSI == null) {
                        continue
                    }
                    if (rssi != lastRSSI) {
                        logger.info { "Signal Strength: $rssi" }
                        textViewRSSI.post { textViewRSSI.text = rssi.toString() }
                        lastRSSI = rssi
                    }
                }
            })
            rssiThread?.name = "Thread-RSSI"
            rssiThread?.start()
        }
/* // Can't get the broadcast receiver for RSSI changed or scan completed working as a better way to get RSSI
        val rssiReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                when (intent?.action){
                    WifiManager.SCAN_RESULTS_AVAILABLE_ACTION -> {
                        val wifi: WifiManager = context?.applicationContext?.getSystemService(android.content.Context.WIFI_SERVICE) as WifiManager
                        val rssi: Int = wifi.connectionInfo.rssi
                        logger.info { "Signal Strength $rssi"}
                        textViewRSSI.post { textViewRSSI.text = rssi.toString()}
                        wifi.startScan()
                    }
                }
            }
        }
        LocalBroadcastManager.getInstance(context).registerReceiver(rssiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
        (context?.applicationContext?.getSystemService(android.content.Context.WIFI_SERVICE) as WifiManager).startScan()
*/

        // Setup the runnables to handle the iperf processes
        iperfTCPRunnable = IperfRunnable(activity.applicationContext,
                object : IperfRunnable.ProcessInterface {
                    override val port = (activity as MainActivity).getSetting(MainActivity.Settings.TCP_PORT) as Int
                    override val protocol = IperfRunnable.Protocol.TCP
                    override val version = (activity as MainActivity).getSetting(MainActivity.Settings.IPERF_VERSION) as Int
                    override fun threadFinish(returnValue: Int, runnable: IperfRunnable) {
                        onThreadExit(returnValue, runnable)
                    }
                    override fun newLine(line: String) {
                        updateLine(line)
                    }
                })

        iperfUDPRunnable = IperfRunnable(activity.applicationContext,
                object : IperfRunnable.ProcessInterface {
                    override val port = (activity as MainActivity).getSetting(MainActivity.Settings.UDP_PORT) as Int
                    override val protocol = IperfRunnable.Protocol.UDP
                    override val version = (activity as MainActivity).getSetting(MainActivity.Settings.IPERF_VERSION) as Int
                    override fun threadFinish(returnValue: Int, runnable: IperfRunnable) {
                        onThreadExit(returnValue, runnable)
                    }
                    override fun newLine(line: String) {
                        updateLine(line)
                    }
                })

        return view
    }

    override fun onStop() {
        super.onStop()
        if (mTestInProgress)
        {
            stopIperf()
        }
    }

    override fun onClick(v: View) {

        if (mTestInProgress)
        {
            stopIperf()
        }
        else if (checkIperf())
        {
            startIperf()
        }
    }

    fun updateLine(line: String) {
        textViewOutput.post(object:Runnable {
            override fun run(){
                textViewOutput.append(line + '\n')
            }
        })
    }

    fun onThreadExit(returnValue: Int, runnable: IperfRunnable){
        logger.debug { "Iperf thread ${runnable.processInterface.protocol} exited" }
        // Restart failed processes if the test is still going on
        if (mTestInProgress && returnValue != 0){
            when(runnable){
                iperfTCPRunnable -> Thread(iperfTCPRunnable).start()
                iperfUDPRunnable -> Thread(iperfUDPRunnable).start()
            }
        }
    }

    fun checkIperf(): Boolean{
        val cm : ConnectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm.activeNetworkInfo?.isConnected != true)
        {
            Toast.makeText(context,"Not connected to network.", Toast.LENGTH_SHORT).show()
            return false
        }

        if ((activity as MainActivity).getSetting(MainActivity.Settings.HOSTNAME).equals(getText(R.string.default_hostname)))
        {
            Toast.makeText(context, "Device hostname not set", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }

    fun startIperf() : Boolean{
        logger.info {"Starting iperf"}

        // Don't allow multiple instances to run at once
        if (iperfTCPThread?.isAlive == true || iperfUDPThread?.isAlive == true)
        {
            return false
        }

        this.view.findViewById<Button>(R.id.buttonStart).text = getText(R.string.button_start_pressed)
        mTestInProgress = true

        iperfTCPThread = Thread(iperfTCPRunnable)
        iperfTCPThread?.start()

        // Only need a separate UDP for iperf 2. Iperf3 can handle both simultaneously.
        if (iperfUDPRunnable?.processInterface?.version == 2) {
            Thread.sleep(1) // Slight delay to ensure the two aren't printing on top of each other
            iperfUDPThread = Thread(iperfUDPRunnable)
            iperfUDPThread?.start()
        }
        return true
    }

    fun stopIperf() {
        logger.info {"Stopping iperf"}
        this.view.findViewById<Button>(R.id.buttonStart).text = getText(R.string.button_start_unpressed)
        mTestInProgress = false

        iperfTCPRunnable?.stopProcess()
        iperfUDPRunnable?.stopProcess()
    }

    fun checkServerConnection(): Boolean {
        val serverAddress: String = PreferenceManager.getDefaultSharedPreferences(activity).getString("textpreference_server", "")
        return isPortOpen(serverAddress, 5201, 250)
    }

    fun isPortOpen(address: String, port: Int, timeout: Int): Boolean {

        try {
            var testSocket: Socket = Socket()
            testSocket.connect(InetSocketAddress(address, port), timeout)
            testSocket.close()
            return true
        }
        catch(e: Exception)
        {
            return false
        }
    }
}
