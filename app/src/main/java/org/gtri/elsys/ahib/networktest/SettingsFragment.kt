package org.gtri.elsys.ahib.networktest


import android.app.Activity
import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceFragment
import android.content.SharedPreferences
import android.preference.EditTextPreference
import android.preference.ListPreference


class SettingsFragment : PreferenceFragment() , SharedPreferences.OnSharedPreferenceChangeListener{

    //private val mListPreference: ListPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Register handler for any changes the user makes to the preferences
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)

        addPreferencesFromResource(R.xml.prefrences)

        // Set the summary for all preferences that use it to show their contents
        for ((key) in preferenceManager.sharedPreferences.all)
        {
            val preference: Preference? = findPreference(key)
            when (preference)
            {
                is EditTextPreference -> preference.summary = preference.text
                is ListPreference -> preference.summary = preference.value
            }
        }


    }
    /*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mListPreference = (ListPreference)  getPreferenceManager().findPreference("preference_key");
        mListPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                // your code here
                return true;
            }
        }

        return inflater.inflate(R.layout.fragment_settings, container, false);
    }*/


    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        val preference: Preference? = findPreference(key)
        when (preference)
        {
            is ListPreference, is EditTextPreference -> preference.summary = preferenceManager.sharedPreferences.getString(key, "Default")
        }
        when(preference?.key)
        {
            "textpreference_server", "textpreference_hostname" ->
            {
                val main: Activity = activity
                if (main is MainActivity)
                {
                    main.updateSubtitle()
                }
            }
        }
    }

    private fun updatePreference(preference: Preference?, key: String) {
/*        if (preference == null) return
        if (preference is ListPreference) {
            val listPreference = preference as ListPreference?
            listPreference!!.summary = listPreference.entry
            return
        }
        val sharedPrefs = preferenceManager.sharedPreferences
        preference.summary = sharedPrefs.getString(key, "Default")
*/

    }
}
