package org.gtri.elsys.ahib.networktest

import android.content.Context
import mu.KLogging
import java.io.*


class IperfRunnable : Runnable {
    companion object: KLogging()

    enum class Protocol{
        TCP, UDP
    }

    interface ProcessInterface
    {
        fun newLine(line: String)
        fun threadFinish(returnValue: Int, runnable: IperfRunnable)
        val port: Int
        val protocol: Protocol
        val version: Int
    }

    var processInterface: ProcessInterface

    private var context: Context
    private var process: Process? = null

    constructor(context: Context, data: ProcessInterface)
    {
        this.context = context
        this.processInterface = data
    }

    override fun run()
    {
        initializeIperf()

        logger.info {"Starting ${processInterface.protocol} iperf server port:${processInterface.port}"}

        val processBuilder = ProcessBuilder().command(constructCommand()).redirectErrorStream(true)

        while (process == null) {
            try {
                process = processBuilder.start()
            } catch (e: Exception){
                logger.warn { "Exception starting iperf: ${e.toString()}" }
            }
        }

        val stdout: BufferedReader = BufferedReader(InputStreamReader(process?.inputStream))

        var line: String? = stdout.readLine()
        while( line != null)
        {
            logger.info { "$line" }
            processInterface.newLine(line)
            line = stdout.readLine()
        }

        val returnValue: Int =  process?.waitFor() ?: -1
        logger.info { "iperf exited; return code $returnValue" }
        processInterface.threadFinish(returnValue, this)
    }

    fun stopProcess()
    {
        // Destroying the process is the only thing that seems to work
        // Closing the stream or reader here will just block until the current readLine returns
        process?.destroy()
        process = null
    }

    private fun constructCommand(): List<String>
    {
        var arguments: ArrayList<String> = arrayListOf<String>()

        arguments.add(context.applicationInfo.dataDir + "/iperf")
        arguments.add("-s")
        if (this.processInterface.port > 0)
        {
            arguments.add("-p")
            arguments.add(this.processInterface.port.toString())
        }
        if (this.processInterface.protocol == Protocol.UDP && processInterface.version == 2)
        {
            arguments.add("-u")
        }
        arguments.add("-i")
        arguments.add("1")

        //arguments.add("-V")
        //arguments.add("-d")

        return arguments
    }

    // Setup everything needed to be able to run iperf
    // Copies the executable from resources to the App directory so it can be run
    private fun initializeIperf() {
        val iperfPath = context.applicationInfo.dataDir + "/iperf"
        val iperfLocal = File(iperfPath)

        val fileIn: InputStream = context.assets.open("iperf${processInterface.version}")

        // Compare the source and destination iperf by size to see if it has already been copied
        // Good enough as it is just the difference between iperf2 and iperf3
        if (iperfLocal.length().compareTo(fileIn.available()) == 0)
        {
            return
        }

        val fileOut: FileOutputStream = FileOutputStream(iperfPath)

        // Copy the resource version of iperf to local app directory
        val buff = ByteArray(1024)
        var read = 0

        try {
            read = fileIn.read(buff)
            while (read > 0) {
                fileOut.write(buff, 0, read)
                read = fileIn.read(buff)
            }
            /*
            while ((read = fileIn.read(buff)) > 0) {
                fileOut.write(buff, 0, read)
            }*/
        } finally {
            fileIn.close()
            fileOut.close()
        }

        iperfLocal.setExecutable(true)
    }
}