package org.gtri.elsys.ahib.networktest

import android.app.FragmentManager
import android.content.Context
import android.content.SharedPreferences
import android.net.wifi.WifiManager
import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceFragment
import android.preference.PreferenceManager
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import mu.KLogging

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){
    companion object: KLogging()

    enum class Settings{
        HOSTNAME, SERVER_ADDRESS, TCP_PORT, UDP_PORT, IPERF_VERSION
    }


    private val preferencesFragment : SettingsFragment = SettingsFragment()
    private val mainFragment : MainFragment = MainFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        logger.info { "Starting MainActivity" }

        fragmentManager.beginTransaction().add(R.id.fragment_container, preferencesFragment).hide(preferencesFragment).commit()
        fragmentManager.executePendingTransactions()
        fragmentManager.beginTransaction().remove(preferencesFragment).commit()

        updateSubtitle()
        setHostname()

        val wifi : WifiManager = this.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifi.isWifiEnabled = true

        fragmentManager.beginTransaction().add(R.id.fragment_container, mainFragment).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                fragmentManager.beginTransaction().replace(R.id.fragment_container, preferencesFragment).addToBackStack(null).commit()
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                true
            }
            R.id.action_close -> {
                finish()
                System.exit(0)
                true
            }
            android.R.id.home -> {
                fragmentManager.popBackStackImmediate()
                if (fragmentManager.backStackEntryCount == 0)
                {
                    supportActionBar?.setDisplayHomeAsUpEnabled(false)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }



    fun getSetting(setting: Settings): Any {
        val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        return when (setting){
            Settings.HOSTNAME -> preferences.getString("textpreference_hostname", "")
            Settings.SERVER_ADDRESS -> preferences.getString("textpreference_server", "")
            Settings.TCP_PORT -> preferences.getString("textpreference_tcp_port", "0").toInt()
            Settings.UDP_PORT -> preferences.getString("textpreference_udp_port", "0").toInt()
            Settings.IPERF_VERSION -> preferences.getString("listpreference_iperf_version", "2").toInt()
        }
    }

    fun updateSubtitle()
    {
        val wifi: WifiManager = applicationContext.getSystemService(android.content.Context.WIFI_SERVICE) as WifiManager
        val local: String = Regex("ipaddr (\\d+\\.\\d+\\.\\d+\\.\\d+)").find(wifi.dhcpInfo.toString())?.groups?.get(1)?.value ?: "0.0.0.0"
        val server: String = Regex("DHCP server (\\d+\\.\\d+\\.\\d+\\.\\d+)").find(wifi.dhcpInfo.toString(),0)?.groups?.get(1)?.value ?: "0.0.0.0"

        // Display the current hostname and server as the subtitle. For the server, use the DHCP server.
        toolbar.subtitle = "Local:${getSetting(Settings.HOSTNAME)}($local) -> $server"
    }

    fun setHostname()
    {
        Thread(HostnameRunnable(getSetting(Settings.HOSTNAME) as String)).start()
    }

}
